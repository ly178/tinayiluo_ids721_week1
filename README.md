[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week1/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week1/-/commits/main)

# Luopeiwen (Tina) Yi's Portfolio Web App 

## Goal
Create a static site with Zola, a Rust static site generator styled with CSS, that holds all of the portofolio work in this class. 

## Preparation
1. Install Zola 
2. Create website with `zola init`
3. Add a theme for css `cd themes` then `git submodule add <theme name>`
4. Create pages in the `content` folder 
5. `zola serve` to test website or `zola build`
6. Install Vercel CLI 
7. Login to Vercel with `vercel login`
8. Link repo with `vercel link`
9. Copy Vercel token, Vercel team id, and Vercel Org id to Gitlab secrets 
10. Push repo to run pipeline filr `.gitlab-ci.yml`

## Portfolio Content

1. Nutrition Guide Web App
2. Rust CLI Binary With SQLite
3. Auto Scaling Flask App
4. Databricks ETL-Pipeline

## Web App Delivery 

[Luopeiwen (Tina) Yi's Portfolio Webiste](https://tinaportfolio.vercel.app/)

![Screen_Shot_2024-01-28_at_5.24.13_PM](/uploads/c997c0ef42d2124959478f49776a1115/Screen_Shot_2024-01-28_at_5.24.13_PM.png)
