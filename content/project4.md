+++
title = "Nutirtion Guide Web App"
date = "2023-12-10"
description = "Data Engineering Final Project"
+++

The team project, "Nutrition Guide," is a dynamic, auto-scaling web application microservice that interfaces with the Databricks data pipeline, utilizing Docker for effective containerization and Azure App Services and Flask for deployment and management.

Explore the user journey within the Nutrition Guide application:

1. Nutrient Selection: Users select their desired nutrient from a drop-down menu within the web application.

2. Nutrition Query: Upon selection, the application processes this input and searches its database to identify the top 10 foods that are highest in the chosen nutrient.

3. Results Display: The web application then displays a detailed list of these top 10 foods, providing users with valuable information about their nutrient content.

Here is our [Nutrition Guide Web Application](https://nutfood.azurewebsites.net).

Here is our [Demo Video](https://youtu.be/64BTJh-Hp-E) showing a clear, concise walkthrough and demonstration of this project.

Here is our [Nutirtion Guide Web App](https://github.com/tinayiluo0322/Nutrition_Guide_Web_App.git) Github project repository. 


