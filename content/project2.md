+++
title = "Databricks ETL-Pipeline "
date = "2023-11-10"
description = "Data Engineering Individual Project 2"
+++

This project develops a Databricks ETL(Extract, Transform, Load) Pipeline for retrieving and processing airline safety datasets, featuring a well-documented notebook for ETL operations, Delta Lake for storage, Spark SQL for transformations, and data visualizations for actionable insights. It ensures data integrity through robust error handling and data validation. An automated Databricks API trigger highlights the focus on automation and continuous processing.

The workflow includes running a Makefile to perform tasks such as installation (make install), testing (make test), code formatting (make format) with Python Black, linting (make lint) with Ruff, and an all-inclusive task (make all) using Github Actions. This automation streamlines the data analysis process and enhances code quality.

Here is my [Demo Video - Databricks ETL Pipeline For Airline Safety Analysis](https://youtu.be/8PpYg7W0Ocg) showing a clear, concise walkthrough and demonstration of this project.

Here is my [Databricks ETL Pipeline](https://github.com/nogibjj/tinayiluo_Databricks_ETL_Pipeline.git) Github project repository. 