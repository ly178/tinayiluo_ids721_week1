+++
title = "Auto Scaling Flask App"
date = "2023-12-06"
description = "Data Engineering Individual Project 3"
+++

The project, "Hello Doctor," is a Flask web application integrated with OpenAI's LLM model, utilizing Docker for effective containerization. Here is a brief workflow overview of the Flask web application:

1. User Input: Individuals input their symptoms into the application.

2. Illness Search: The application processes these symptoms and searches for a corresponding illness using the AI model.

3. Diagnostic Output: The web application then presents a detailed diagnostic of the identified illness to the user.

Here is my [Hello Doctor Web Application](https://week13.calmisland-989659dc.westus2.azurecontainerapps.io).

Here is my [Demo_Video](https://youtu.be/3LT7dBMBwjU)showing a clear, concise walkthrough and demonstration of this project.

Here is my [Auto Scaling Flask App](https://github.com/nogibjj/tinayiluo_Auto_Scaling_Flask_App.git) Github project repository. 